# Retail Banking - Account Management System

## Introduction

This is a microservice designed for a retail bank that provides functionalities related to account management. It is designed to replace a monolithic application with a more modern, maintainable and scalable architecture.

## Functionalities

The system provides the following functionalities:

1. **Create a Savings Account for the Customer**: The system allows the creation of a new savings account for an existing customer.
2. **Deposit Money to Account**: The system allows a specified amount of money (e.g., $100) to be deposited into an existing account.
3. **Withdraw Money from Account**: The system allows a specified amount of money (e.g., $50) to be withdrawn from an existing account. If there is not enough balance in the account, the system will deny the withdrawal request.
4. **Read Available Balance**: The system allows querying of the available balance in a specified account.
5. **List Last 10 Transactions of the Account**: The system provides the ability to retrieve the last 10 transactions that occurred on a specified account.

## Note

This application is designed to be part of a microservice architecture and focuses on the Account domain. Other domains, such as the Customer domain, are assumed to be managed by other systems or applications within the bank.


## API Endpoints

1. **Create New Account**: `POST /accounts`
   - Request body: CreateNewAccountRequest object
   - Response: CreateNewAccountResponse object
2. **Deposit to Account**: `POST /accounts/deposit`
   - Request body: TransactionRequest object for deposit
   - Response: TransactionResponse object
3. **Withdraw from Account**: `POST /accounts/withdraw`
   - Request body: TransactionRequest object for withdrawal
   - Response: TransactionResponse object
4. **Get Account Balance**: `POST /accounts/balance`
   - Request body: GetBalanceRequest object
   - Response: GetBalanceResponse object
5. **Find Transactions by Account ID**: `POST /transactions`
   - Request body: FindTransactionsRequest object
   - Response: List of TransactionResponse objects

For more details about the request and response objects, please refer to the code.

## Technologies Used

1. Java 17
2. Junit 5
3. Spring-boot
4. Maven
