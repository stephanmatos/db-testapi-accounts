package com.debt.dbtestapi.service;

import com.debt.dbtestapi.model.account.Account;
import com.debt.dbtestapi.repository.AccountRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AccountFinderServiceTest {

    @Mock
    private AccountRepository accountRepository;

    @InjectMocks
    private AccountFinderService accountFinderService;

    @Test
    public void testGetAccountById_found() {
        long id = 1L;
        Account expectedAccount = Account.builder().id(id).build();  
        when(accountRepository.findById(id)).thenReturn(Optional.of(expectedAccount));

        Account actualAccount = accountFinderService.getAccountById(id);

        assertEquals(expectedAccount, actualAccount);
        verify(accountRepository, times(1)).findById(id);
    }

    @Test
    public void testGetAccountById_notFound() {
        long id = 1L;
        when(accountRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(ResponseStatusException.class, () -> accountFinderService.getAccountById(id));

        verify(accountRepository, times(1)).findById(id);
    }
}
