package com.debt.dbtestapi.integration;

import com.debt.dbtestapi.dto.request.CreateNewAccountRequest;
import com.debt.dbtestapi.model.account.Account;
import com.debt.dbtestapi.model.currency.Currency;
import com.debt.dbtestapi.model.customer.Customer;
import com.debt.dbtestapi.repository.AccountRepository;
import com.debt.dbtestapi.repository.CustomerRepository;
import com.debt.dbtestapi.service.AccountService;
import com.debt.dbtestapi.service.CurrencyFinderService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CreateNewAccountTest {

    @SpyBean
    private AccountService accountService;

    @MockBean
    private CustomerRepository customerRepository;

    @MockBean
    private CurrencyFinderService currencyFinderService;

    @MockBean
    private AccountRepository accountRepository;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testCreateNewAccount() throws Exception {
        Long customerId = 1L;
        Long currencyId = 1L;

        CreateNewAccountRequest request = CreateNewAccountRequest.builder()
                .customerId(customerId)
                .currencyId(currencyId)
                .build();

        Customer mockCustomer = new Customer();
        Currency mockCurrency = new Currency();
        Account mockAccount = new Account();
        mockAccount.setId(1L);
        mockAccount.setCustomer(mockCustomer);
        mockAccount.setCurrency(mockCurrency);
        mockAccount.setBalance(BigDecimal.ZERO);

        when(customerRepository.findById(customerId)).thenReturn(Optional.of(mockCustomer));
        when(currencyFinderService.getCurrencyById(currencyId)).thenReturn(mockCurrency);
        when(accountService.createNewAccount(request)).thenReturn(mockAccount);

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/accounts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isOk());

        verify(accountRepository, times(1)).save(any(Account.class));
    }
}
