package com.debt.dbtestapi.controller;

import com.debt.dbtestapi.dto.request.CreateNewAccountRequest;
import com.debt.dbtestapi.dto.request.GetBalanceRequest;
import com.debt.dbtestapi.dto.request.TransactionRequest;
import com.debt.dbtestapi.model.account.Account;
import com.debt.dbtestapi.model.currency.Currency;
import com.debt.dbtestapi.model.customer.Customer;
import com.debt.dbtestapi.model.enums.CurrencyType;
import com.debt.dbtestapi.model.transaction.Transaction;
import com.debt.dbtestapi.service.AccountFinderService;
import com.debt.dbtestapi.service.AccountService;
import com.debt.dbtestapi.service.AccountTransactionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(MockitoExtension.class)
public class AccountControllerUnitTest {

    @Mock
    private AccountService accountService;

    @Mock
    private AccountTransactionService accountTransactionService;

    @Mock
    private AccountFinderService accountFinderService;

    @InjectMocks
    private AccountController accountController;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(accountController).build();
    }

    @Test
    void testCreateNewAccount() throws Exception {
        CreateNewAccountRequest request = CreateNewAccountRequest.builder().customerId(1L).currencyId(3L).build();
        Account account = Account.builder()
                .id(2L)
                .customer(Customer.builder()
                        .id(1L)
                        .build())
                .currency(Currency.builder()
                        .id(3L)
                        .build())
                .build();

        when(accountService.createNewAccount(any(CreateNewAccountRequest.class))).thenReturn(account);

        mockMvc.perform(post("/accounts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(request)))
                .andExpect(status().isOk());

        verify(accountService, times(1)).createNewAccount(any(CreateNewAccountRequest.class));
    }

    @Test
    void testDepositToAccount() throws Exception {
        TransactionRequest request = TransactionRequest.builder()
                .customerId(1L)
                .accountId(2L)
                .currencyId(3L)
                .build();

        Transaction transaction = Transaction.builder()
                .id(1L)
                .account(Account.builder()
                        .id(2L)
                        .build())
                .currency(Currency.builder()
                        .id(3L)
                        .currencyType(CurrencyType.DKK)
                        .build())
                .build();

        when(accountTransactionService.depositToAccount(any(TransactionRequest.class))).thenReturn(transaction);

        mockMvc.perform(post("/accounts/deposit") // Replace with your actual endpoint
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(request)))
                .andExpect(status().isOk());

        verify(accountTransactionService, times(1)).depositToAccount(any(TransactionRequest.class));
    }

    @Test
    void testWithdrawFromAccount() throws Exception {
        TransactionRequest request = TransactionRequest.builder()
                .customerId(1L)
                .accountId(2L)
                .currencyId(3L)
                .build();

        Transaction transaction = Transaction.builder()
                .id(1L)
                .account(Account.builder()
                        .id(2L)
                        .build())
                .currency(Currency.builder()
                        .id(3L)
                        .build())
                .build();

        when(accountTransactionService.withdrawFromAccount(any(TransactionRequest.class))).thenReturn(transaction);

        mockMvc.perform(post("/accounts/withdraw") // Replace with your actual endpoint
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(request)))
                .andExpect(status().isOk());

        verify(accountTransactionService, times(1)).withdrawFromAccount(any(TransactionRequest.class));
    }

    @Test
    void testGetAccountBalance() throws Exception {
        GetBalanceRequest request = GetBalanceRequest.builder().accountId(1L).build();
        Account account = Account.builder()
                .id(1L)
                .customer(Customer.builder()
                        .id(1L)
                        .build())
                .currency(Currency.builder()
                        .id(3L)
                        .build())
                .build();

        when(accountFinderService.getAccountById(anyLong())).thenReturn(account);

        mockMvc.perform(post("/accounts/balance", request.getAccountId())
                        .content(new ObjectMapper().writeValueAsString(request))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(accountFinderService, times(1)).getAccountById(anyLong());
    }

}
