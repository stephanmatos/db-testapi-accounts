package com.debt.dbtestapi.model.account;

import com.debt.dbtestapi.dto.request.TransactionRequest;
import com.debt.dbtestapi.model.currency.Currency;
import com.debt.dbtestapi.model.customer.Customer;
import com.debt.dbtestapi.model.enums.TransactionType;
import com.debt.dbtestapi.model.transaction.Transaction;
import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.util.List;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Entity
@Data
@NoArgsConstructor
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Customer customer;

    private BigDecimal balance;

    @ManyToOne
    private Currency currency;

    @OneToMany
    private List<Transaction> transactions;

    public static Account createAccount(Customer customer, Currency currency) {
        return Account.builder()
                .customer(customer)
                .currency(currency)
                .balance(BigDecimal.ZERO)
                .build();
    }

    public void updateBalance(TransactionRequest transactionRequest) {
        if (transactionRequest.getTransactionType() == TransactionType.DEPOSIT) {
            balance = balance.add(transactionRequest.getAmount());
        } else if (transactionRequest.getTransactionType() == TransactionType.WITHDRAWAL) {
            balance = balance.subtract(transactionRequest.getAmount());
        } else {
            throw new UnsupportedOperationException("Invalid transaction type");
        }
    }


}