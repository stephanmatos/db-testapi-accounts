package com.debt.dbtestapi.model.transaction;


import com.debt.dbtestapi.model.account.Account;
import com.debt.dbtestapi.model.currency.Currency;
import com.debt.dbtestapi.model.enums.TransactionType;
import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Entity
@Data
@NoArgsConstructor
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Account account;

    private TransactionType transactionType;

    private BigDecimal amount;

    @ManyToOne
    private Currency currency;

    private LocalDateTime timestamp;
}
