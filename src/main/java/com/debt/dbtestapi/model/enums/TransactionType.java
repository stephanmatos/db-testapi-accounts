package com.debt.dbtestapi.model.enums;

public enum TransactionType {
    DEPOSIT, WITHDRAWAL, TRANSFER
}
