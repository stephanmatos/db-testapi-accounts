package com.debt.dbtestapi.model.enums;

public enum AccountType {
    BUDGET("Budget Account"),
    CHECKING("Checking Account"),
    INVESTMENT("Investment Account"),
    SAVINGS("Savings Account")
    ;

    private final String displayName;

    AccountType(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return this.displayName;
    }
}