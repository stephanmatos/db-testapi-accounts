package com.debt.dbtestapi.model.currency;

import com.debt.dbtestapi.model.enums.CurrencyType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Entity
@Data
@NoArgsConstructor
public class Currency {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private CurrencyType currencyType;

    public void checkCurrency(Currency inputCurrency) {
        if (!this.getCurrencyType().equals(inputCurrency.getCurrencyType())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Mismatch between currency on account and input currency");
        }
    }

}
