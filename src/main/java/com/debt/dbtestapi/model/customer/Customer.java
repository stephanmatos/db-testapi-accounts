package com.debt.dbtestapi.model.customer;

import com.debt.dbtestapi.model.account.Account;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.*;

import java.util.List;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Data
@Entity
@NoArgsConstructor
public class Customer {
    @Id
    private Long id;

    @OneToMany
    private List<Account> accountList;

}
