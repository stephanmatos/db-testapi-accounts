package com.debt.dbtestapi.dto.request;

import lombok.*;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Data
@NoArgsConstructor
public class FindTransactionsRequest {

    @NonNull
    private Long accountId;
    @NonNull
    private Integer count;
}
