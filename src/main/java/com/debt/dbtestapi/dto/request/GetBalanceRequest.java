package com.debt.dbtestapi.dto.request;

import lombok.*;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Data
@NoArgsConstructor
public class GetBalanceRequest {

    @NonNull
    private Long accountId;

}
