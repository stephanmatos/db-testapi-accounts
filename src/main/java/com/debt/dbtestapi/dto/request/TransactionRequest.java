package com.debt.dbtestapi.dto.request;

import com.debt.dbtestapi.model.enums.TransactionType;
import lombok.*;

import java.math.BigDecimal;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Data
@NoArgsConstructor
public class TransactionRequest {
    @NonNull
    private Long customerId;
    @NonNull
    private Long accountId;
    @NonNull
    private Long currencyId;
    private BigDecimal amount;
    private TransactionType transactionType;


}
