package com.debt.dbtestapi.dto.request;

import com.debt.dbtestapi.model.enums.AccountType;
import lombok.*;


@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Data
@NoArgsConstructor
public class CreateNewAccountRequest {

    @NonNull
    private Long customerId;
    @NonNull
    private Long currencyId;
    private AccountType accountType;

}
