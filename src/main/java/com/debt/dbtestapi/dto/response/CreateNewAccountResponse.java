package com.debt.dbtestapi.dto.response;

import com.debt.dbtestapi.model.account.Account;
import lombok.*;

import java.math.BigDecimal;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Data
@NoArgsConstructor
public class CreateNewAccountResponse {

    private Long accountId;
    private Long currencyId;
    private Long customerId;
    private BigDecimal balance;

    public static CreateNewAccountResponse fromAccount(Account account) {
        return CreateNewAccountResponse.builder()
                .accountId(account.getId())
                .currencyId(account.getCurrency().getId())
                .customerId(account.getCustomer().getId())
                .balance(account.getBalance())
                .currencyId(account.getCurrency().getId())
                .build();
    }
}
