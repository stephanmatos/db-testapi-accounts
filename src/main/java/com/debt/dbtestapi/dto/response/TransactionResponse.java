package com.debt.dbtestapi.dto.response;

import com.debt.dbtestapi.model.enums.TransactionType;
import com.debt.dbtestapi.model.transaction.Transaction;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Data
@NoArgsConstructor
public class TransactionResponse {

    private Long transactionId;
    private Long accountId;
    private Long currencyId;
    private BigDecimal amount;
    private TransactionType transactionType;
    private LocalDateTime timestamp;

    public static TransactionResponse fromTransaction(Transaction transaction) {
        return TransactionResponse.builder()
                .transactionId(transaction.getId())
                .accountId(transaction.getAccount().getId())
                .currencyId(transaction.getCurrency().getId())
                .amount(transaction.getAmount())
                .transactionType(transaction.getTransactionType())
                .timestamp(transaction.getTimestamp())
                .build();
    }

}
