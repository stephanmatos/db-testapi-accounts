package com.debt.dbtestapi.dto.response;

import com.debt.dbtestapi.model.account.Account;
import lombok.*;

import java.math.BigDecimal;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Data
@NoArgsConstructor
public class GetBalanceResponse {

    private Long accountId;
    private Long currencyId;
    private Long customerId;
    private BigDecimal balance;

    public static GetBalanceResponse fromAccount(Account account) {
        return GetBalanceResponse.builder()
                .accountId(account.getId())
                .currencyId(account.getCurrency().getId())
                .customerId(account.getCustomer().getId())
                .balance(account.getBalance())
                .build();
    }
}
