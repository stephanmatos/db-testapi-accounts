package com.debt.dbtestapi.service;

import com.debt.dbtestapi.dto.request.CreateNewAccountRequest;
import com.debt.dbtestapi.model.account.Account;
import com.debt.dbtestapi.model.currency.Currency;
import com.debt.dbtestapi.model.customer.Customer;
import com.debt.dbtestapi.repository.AccountRepository;
import com.debt.dbtestapi.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import static com.debt.dbtestapi.model.account.Account.createAccount;

@RequiredArgsConstructor
@Service
@Transactional
public class AccountService {

    private final AccountRepository accountRepository;
    private final CurrencyFinderService currencyFinderService;
    private final CustomerRepository customerRepository;

    public Account createNewAccount(CreateNewAccountRequest createNewAccountRequest) {
        Customer customer = customerRepository.findById(createNewAccountRequest.getCustomerId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Error - Customer ID not found"));

        Currency currency = currencyFinderService.getCurrencyById(createNewAccountRequest.getCurrencyId());
        Account account = createAccount(customer, currency);

        save(account);

        return account;
    }

    public void save(Account account) {
        accountRepository.save(account);
    }


}
