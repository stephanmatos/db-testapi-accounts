package com.debt.dbtestapi.service;

import com.debt.dbtestapi.dto.request.FindTransactionsRequest;
import com.debt.dbtestapi.model.transaction.Transaction;
import com.debt.dbtestapi.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RequiredArgsConstructor
@Service
@Transactional
public class TransactionFinderService {

    private final TransactionRepository transactionRepository;

    public List<Transaction> findTransactionsByAccountId(FindTransactionsRequest findTransactionsRequest) {
        Pageable pageable = PageRequest.of(0, findTransactionsRequest.getCount(), Sort.by("timestamp").descending());

        Page<Transaction> transactionPage = transactionRepository.findByAccountId(findTransactionsRequest.getAccountId(), pageable);

        return transactionPage.getContent();
    }

}
