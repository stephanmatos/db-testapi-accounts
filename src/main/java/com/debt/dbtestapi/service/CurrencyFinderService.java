package com.debt.dbtestapi.service;

import com.debt.dbtestapi.model.currency.Currency;
import com.debt.dbtestapi.repository.CurrencyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

@RequiredArgsConstructor
@Service
@Transactional
public class CurrencyFinderService {

    private final CurrencyRepository currencyRepository;

    public Currency getCurrencyById(long id) {
        return currencyRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
}
