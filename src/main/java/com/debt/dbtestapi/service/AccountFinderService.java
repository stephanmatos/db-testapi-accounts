package com.debt.dbtestapi.service;

import com.debt.dbtestapi.model.account.Account;
import com.debt.dbtestapi.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

@RequiredArgsConstructor
@Service
@Transactional
public class AccountFinderService {

    private final AccountRepository accountRepository;

    public Account getAccountById(long id) {
        return accountRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }


}
