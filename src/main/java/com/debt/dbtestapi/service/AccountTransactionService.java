package com.debt.dbtestapi.service;

import com.debt.dbtestapi.dto.request.TransactionRequest;
import com.debt.dbtestapi.model.account.Account;
import com.debt.dbtestapi.model.currency.Currency;
import com.debt.dbtestapi.model.enums.TransactionType;
import com.debt.dbtestapi.model.transaction.Transaction;
import com.debt.dbtestapi.repository.AccountRepository;
import com.debt.dbtestapi.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.time.LocalDateTime;


@RequiredArgsConstructor
@Service
@Transactional
public class AccountTransactionService {

    private final AccountFinderService accountFinderService;
    private final AccountService accountService;
    private final AccountRepository accountRepository;
    private final CurrencyFinderService currencyFinderService;
    private final TransactionRepository transactionRepository;


    public Transaction depositToAccount(TransactionRequest depositRequest) {
        return performTransaction(depositRequest, TransactionType.DEPOSIT);
    }

    public Transaction withdrawFromAccount(TransactionRequest withdrawalRequest) {
        if (!isBalanceAvailable(withdrawalRequest.getAccountId(), withdrawalRequest.getAmount())) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Balance not available");
            //Could add some more rules and expand logic by account type and customer type
        }

        return performTransaction(withdrawalRequest, TransactionType.WITHDRAWAL);
    }

    public Transaction performTransaction(TransactionRequest transactionRequest, TransactionType transactionType) {
        Account account = accountFinderService.getAccountById(transactionRequest.getAccountId());
        Currency inputCurrency = currencyFinderService.getCurrencyById(transactionRequest.getCurrencyId());
        account.getCurrency().checkCurrency(inputCurrency);
        account.updateBalance(transactionRequest);
        accountService.save(account);

        Transaction newTransaction = Transaction.builder()
                .account(account)
                .transactionType(transactionType)
                .amount(transactionRequest.getAmount())
                .currency(inputCurrency)
                .timestamp(LocalDateTime.now())
                .build();

        transactionRepository.save(newTransaction);

        return newTransaction;
    }


    private boolean isBalanceAvailable(long accountId, BigDecimal amount) {
        return accountRepository.isBalanceAvailable(accountId, amount);
    }

}
