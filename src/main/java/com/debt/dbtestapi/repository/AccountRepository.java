package com.debt.dbtestapi.repository;

import com.debt.dbtestapi.model.account.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;

public interface AccountRepository extends JpaRepository<Account, Long> {

    @Query("SELECT CASE WHEN (a.balance >= :amount) THEN true ELSE false END FROM Account a WHERE a.id = :accountId")
    boolean isBalanceAvailable(@Param("accountId") Long accountId, @Param("amount") BigDecimal amount);
}
