package com.debt.dbtestapi.repository;

import com.debt.dbtestapi.model.currency.Currency;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyRepository extends JpaRepository<Currency, Long> {
}
