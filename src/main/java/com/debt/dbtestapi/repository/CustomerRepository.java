package com.debt.dbtestapi.repository;

import com.debt.dbtestapi.model.customer.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
