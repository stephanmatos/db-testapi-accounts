package com.debt.dbtestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbTestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbTestapiApplication.class, args);
	}

}
