package com.debt.dbtestapi.controller;

import com.debt.dbtestapi.dto.request.FindTransactionsRequest;
import com.debt.dbtestapi.dto.response.TransactionResponse;
import com.debt.dbtestapi.model.transaction.Transaction;
import com.debt.dbtestapi.service.TransactionFinderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/transactions")
@RequiredArgsConstructor
@RestController
public class TransactionController {

    private final TransactionFinderService transactionFinderService;

    @PostMapping
    public ResponseEntity<List<TransactionResponse>> findTransactionsByAccountId(@RequestBody FindTransactionsRequest findTransactionsRequest) {
        List<Transaction> transactionList = transactionFinderService.findTransactionsByAccountId(findTransactionsRequest);

        List<TransactionResponse> transactionResponses = transactionList.stream().map(TransactionResponse::fromTransaction).toList();

        return ResponseEntity.ok(transactionResponses);
    }


}
