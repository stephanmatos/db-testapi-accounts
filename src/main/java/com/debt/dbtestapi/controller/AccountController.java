package com.debt.dbtestapi.controller;

import com.debt.dbtestapi.dto.request.CreateNewAccountRequest;
import com.debt.dbtestapi.dto.request.GetBalanceRequest;
import com.debt.dbtestapi.dto.request.TransactionRequest;
import com.debt.dbtestapi.dto.response.CreateNewAccountResponse;
import com.debt.dbtestapi.dto.response.GetBalanceResponse;
import com.debt.dbtestapi.dto.response.TransactionResponse;
import com.debt.dbtestapi.model.account.Account;
import com.debt.dbtestapi.model.transaction.Transaction;
import com.debt.dbtestapi.service.AccountFinderService;
import com.debt.dbtestapi.service.AccountService;
import com.debt.dbtestapi.service.AccountTransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/accounts")
@RequiredArgsConstructor
@RestController
public class AccountController {

    private final AccountFinderService accountFinderService;
    private final AccountService accountService;
    private final AccountTransactionService accountTransactionService;

    @PostMapping
    public ResponseEntity<CreateNewAccountResponse> createNewAccount(@RequestBody CreateNewAccountRequest createNewAccountRequest) {
        Account account = accountService.createNewAccount(createNewAccountRequest);

        CreateNewAccountResponse response = CreateNewAccountResponse.fromAccount(account);

        return ResponseEntity.ok(response);
    }

    @PostMapping("/deposit")
    public ResponseEntity<TransactionResponse> depositToAccount(@RequestBody TransactionRequest depositRequest) {
        Transaction transaction = accountTransactionService.depositToAccount(depositRequest);

        TransactionResponse transactionResponse = TransactionResponse.fromTransaction(transaction);

        return ResponseEntity.ok(transactionResponse);
    }

    @PostMapping("/withdraw")
    public ResponseEntity<TransactionResponse> withdrawFromAccount(@RequestBody TransactionRequest withdrawalRequest) {
        Transaction transaction = accountTransactionService.withdrawFromAccount(withdrawalRequest);

        TransactionResponse transactionResponse = TransactionResponse.fromTransaction(transaction);

        return ResponseEntity.ok(transactionResponse);
    }

    @PostMapping("/balance")
    public ResponseEntity<GetBalanceResponse> getAccountBalance(@RequestBody GetBalanceRequest getBalanceRequest) {
        Account account = accountFinderService.getAccountById(getBalanceRequest.getAccountId());

        GetBalanceResponse response = GetBalanceResponse.fromAccount(account);

        return ResponseEntity.ok(response);
    }




}
